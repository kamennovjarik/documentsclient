﻿using documentsClient.DataSources;
using documentsClient.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace documentsClient.view
{
    public partial class form2cs : Form
    {
        paper paper; 
        public form2cs(paper paper)
        {
            this.paper = paper;
            InitializeComponent();
        }

        private void form2cs_Load(object sender, EventArgs e)
        {
            paperName.Text = paper.title;
            authors.Text = paper.authors;
            expertOpinionState.Text = paper.opinionsState.stateExpertOpinion;
            exportOpinionState.Text = paper.opinionsState.stateExportOpinion;
            fio.Text = paper.expert_opinion_owner;
            comment.Text = paper.comment;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private async void confirm_Click(object sender, EventArgs e)
        {
            switch (expertOpinionState.SelectedIndex)     
            {
                case 0:
                    paper.state_expert_opinion = "N";
                    break;
                case 1:
                    paper.state_expert_opinion = "S";
                    break;
                case 2:
                    paper.state_expert_opinion = "E";
                    break;
            }

            switch (exportOpinionState.SelectedIndex)
            {
                case 0:
                    paper.state_export_opinion = "N";
                    break;
                case 1:
                    paper.state_export_opinion = "S";
                    break;
                case 2:
                    paper.state_export_opinion = "E";
                    break;
            }
            paper.authors = authors.Text;
            paper.comment = comment.Text;
            paper.expert_opinion_owner = fio.Text;
            
            await WebConnector.UpdatePaperAsync(paper);
            Close();

        }

    }
}
