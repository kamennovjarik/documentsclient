﻿using documentsClient.DataSources;
using documentsClient.model;
using documentsClient.view;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace documentsClient
{
    public partial class Form1 : Form
    {
        List<paper> papers;
        public Form1()
        {
            InitializeComponent();
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            await updateDGV();
        }

        private async Task updateDGV()
        {
            dataGridView1.Rows.Clear();
            papers = await WebConnector.GetAllPapersAsync();
            papers.ForEach(c => { dataGridView1.Rows.Add(c.id, c.authors, c.title, c.opinionsState.stateExpertOpinion, c.opinionsState.stateExportOpinion);
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[3].Style.BackColor = c.opinionsState.colorExpertOpinion;
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[4].Style.BackColor = c.opinionsState.colorExportOpinion;

            });
        }

        private async void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0) { return; }
            Fields f = new Fields(papers.Find(c => c.id == (int)dataGridView1.SelectedRows[0].Cells[0].Value)); 
            f.ShowDialog();
            await updateDGV();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0) { return; }
            paper paper = papers.Find(c => c.id == (int)dataGridView1.SelectedRows[0].Cells[0].Value);
            form2cs sentForm = new form2cs(paper);
            sentForm.ShowDialog();
            await updateDGV();
        }
    }
}
