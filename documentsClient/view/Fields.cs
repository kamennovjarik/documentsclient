﻿using documentsClient.DataSources;
using documentsClient.model;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace documentsClient.view
{
    public partial class Fields : Form
    {
        paper curPaper;
        public Fields(paper paper)
        {
            InitializeComponent();
            curPaper = paper;
        }

        private void Fields_Load(object sender, EventArgs e)
        {
            authors.Text = curPaper.authors;
            title.Text = curPaper.title;
            source.Text = (curPaper.source.Contains("URL : https:") ? curPaper.source.Substring(0, curPaper.source.IndexOf("URL : https:")) : curPaper.source) + (curPaper.link == null ? "" : ("URL : " + curPaper.link));
            year.Value = curPaper.year;
            templates.DataSource = WebConnector.getTemplates();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            if (!validForm()) {
                return;
            }
            Microsoft.Office.Interop.Word._Application oWord = new Microsoft.Office.Interop.Word.Application();

            // Считывает шаблон и сохраняет измененный в новом
            _Document oDoc = oWord.Documents.Add(Environment.CurrentDirectory + "\\Templates\\" + templates.Text);

            oDoc = addToBookMark(oDoc, "YEAR0", "" + year.Value);
            oDoc = addToBookMark(oDoc, "YEAR1", "" + year.Value);
            oDoc = addToBookMark(oDoc, "YEAR2", "" + year.Value);

            oDoc = addToBookMark(oDoc, "DAY0", "" + day.Value);
            oDoc = addToBookMark(oDoc, "DAY1", "" + day.Value);
            oDoc = addToBookMark(oDoc, "DAY2", "" + day.Value);

            oDoc = addToBookMark(oDoc, "MONTH0", "" + month.Text);
            oDoc = addToBookMark(oDoc, "MONTH1", "" + month.Text);
            oDoc = addToBookMark(oDoc, "MONTH2", "" + month.Text);

            oDoc = addToBookMark(oDoc, "AUTHORIP0", "" + authors.Text);
            oDoc = addToBookMark(oDoc, "AUTHORIP1", "" + authors.Text);
            oDoc = addToBookMark(oDoc, "AUTHORIP2", "" + authors.Text);

            oDoc = addToBookMark(oDoc, "AUTHORSRP0", "" + authorsRP.Text);
            oDoc = addToBookMark(oDoc, "AUTHORSRP1", "" + authorsRP.Text);
            oDoc = addToBookMark(oDoc, "AUTHORSRP2", "" + authorsRP.Text);

            oDoc = addToBookMark(oDoc, "DOCNAME0", "" + title.Text);
            oDoc = addToBookMark(oDoc, "DOCNAME1", "" + title.Text);
            oDoc = addToBookMark(oDoc, "DOCNAME2", "" + title.Text);

            oDoc = addToBookMark(oDoc, "DOCTYPERP0", "" + typeRP.Text);
            oDoc = addToBookMark(oDoc, "DOCTYPERP1", "" + typeRP.Text);
            oDoc = addToBookMark(oDoc, "DOCTYPERP2", "" + typeRP.Text);

            oDoc = addToBookMark(oDoc, "DOCTYPEIP0", "" + typeIP.Text);
            oDoc = addToBookMark(oDoc, "DOCTYPEIP1", "" + typeIP.Text);
            oDoc = addToBookMark(oDoc, "DOCTYPEIP2", "" + typeIP.Text);

            oDoc = addToBookMark(oDoc, "PROTOCOL0", "" + protocolNumber.Text);
            oDoc = addToBookMark(oDoc, "PROTOCOL1", "" + protocolNumber.Text);
            oDoc = addToBookMark(oDoc, "PROTOCOL2", "" + protocolNumber.Text);

            oDoc = addToBookMark(oDoc, "SOURCE0", "" + ITypeRP.Text + " " + source.Text);
            oDoc = addToBookMark(oDoc, "SOURCE1", "" + ITypeRP.Text + " " + source.Text);
            oDoc = addToBookMark(oDoc, "SOURCE2", "" + ITypeRP.Text + " " + source.Text);
            
            try
            {
                oDoc.SaveAs(FileName: Environment.CurrentDirectory + "\\docs\\" + title.Text + " " + templates.Text + ".docx");
                if (templates.Text.Contains("экспертное заключение")) {
                    curPaper.state_expert_opinion = "U";
                    await WebConnector.UpdatePaperAsync(curPaper);
                }
            }
            catch {
                MessageBox.Show("Файл используется");
            }

            oDoc.Close();

            MessageBox.Show("Готово");
        }

        private bool validForm() {
            if (authors.Text == "")
            {
                MessageBox.Show("Автор ИП не заполнено");
                return false;
            }

            if (authorsRP.Text == "")
            {
                MessageBox.Show("Автор РП не заполнено");
                return false;
            }

            if (typeIP.Text == "")
            {
                MessageBox.Show("Тип публикации ИП не заполнено");
                return false;
            }

            if (typeRP.Text == "")
            {
                MessageBox.Show("Тип публикации РП не заполнено");
                return false;
            }

            if (title.Text == "")
            {
                MessageBox.Show("Название публикации не заполнено");
                return false;
            }

            if (month.SelectedIndex == -1)
            {
                MessageBox.Show("Месяц не выбран");
                return false;
            }

            if (ITypeRP.Text == "")
            {
                MessageBox.Show("Тип издания РП не заполнено");
                return false;
            }

            if (ITypeIP.Text == "")
            {
                MessageBox.Show("Тип издания ИП не заполнено");
                return false;
            }

            if (source.Text == "")
            {
                MessageBox.Show("Издание ДП не заполнено");
                return false;
            }

            if (templates.SelectedIndex == -1)
            {
                MessageBox.Show("Шаблон не выбран");
                return false;
            }

            return true;
        }

        private _Document addToBookMark(_Document oDoc, string bookmark, string text) {
            if (oDoc.Bookmarks.Exists(bookmark)) {
                oDoc.Bookmarks[bookmark].Range.Text = text;
            }

            return oDoc;
        }
    }
}
