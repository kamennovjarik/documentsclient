﻿using documentsClient.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace documentsClient.DataSources
{
    public static class WebConnector
    {
        static string bUrl = "http://localhost:3000";

        //Запрос получения всех записей в таблице
        public static async Task<List<paper>> GetAllPapersAsync()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(bUrl + "/paper/all");

            if (!response.IsSuccessStatusCode)
            {
                return new List<paper>();
            }

            string jsonReq = "" + await response.Content.ReadAsStringAsync();
            return JsonConverter.ParcePapersFromJSON(jsonReq);
        }

        //Запрос получения всех записей в таблице
        public static async Task UpdatePaperAsync(paper _paper)
        {
            var request = WebRequest.Create(bUrl + "/paper/update");
            request.ContentType = "application/json";
            request.Method = "PUT";

            string json = JsonConverter.paperToJSON(_paper);
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
            }

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            using (Stream responseStream = response.GetResponseStream())
            {
                return;
            }
        }

        //Заглушка, в будущем шаблоны хранятся не локально
        public static List<String> getTemplates()
        {
            List<String> tmp = new List<String>(Directory.GetFiles("Templates"));
            List<String> res = new List<string>();

            tmp.ForEach(c => res.Add(c.Split('\\')[1]));
            return res;
        }
    }
}
