﻿
using System.Drawing;
using System.Runtime.Serialization;

namespace documentsClient.model
{
    public class opinion
    {
        public string expert_opinion;

        public string export_opinion;
       
        public string expert_opinion_owner;
        
        public string state_expert_opinion;
        
        public string state_export_opinion;

        public string stateExpertOpinion => getStateExpertOpinion();
        public Color colorExpertOpinion => getColorExpertOpinion();

        public string stateExportOpinion => getStateExportOpinion();
        public Color colorExportOpinion => getColorExportOpinion();

        private string getStateExpertOpinion()
        {
            if (state_expert_opinion == "N")
                return "Отсутствует";
            else if (state_expert_opinion == "S")
                return "Сдано";
            else if (state_expert_opinion == "U")
                return "Сгенерировано";
            return "Не нужно";
        }

        private Color getColorExpertOpinion()
        {
            if (state_expert_opinion == "N")
                return Color.Red;
            else if (state_expert_opinion == "S")
                return Color.Green;
            else if (state_expert_opinion == "U")
                return Color.Yellow;
            return Color.Blue;
        }

        private string getStateExportOpinion()
        {
            if (state_export_opinion == "N")
                return "Отсутствует";
            else if (state_export_opinion == "S")
                return "Сдано";
            else if (state_export_opinion == "U")
                return "Сгенерировано";
            return "Не нужно";
        }

        private Color getColorExportOpinion()
        {
            if (state_export_opinion == "N")
                return Color.Red;
            else if (state_export_opinion == "S")
                return Color.Green;
            else if (state_export_opinion == "U")
                return Color.Yellow;
            return Color.Blue;
        }
    }
}
