﻿using System.Runtime.Serialization;

namespace documentsClient.model
{
    [DataContract]
    public class paper
    {
        [DataMember]
        public int id;
        [DataMember]
        public string title;
        [DataMember]
        public string source;
        [DataMember]
        public string authors;
        [DataMember]
        public string link;
        [DataMember]
        public string expert_opinion;
        [DataMember]
        public string export_opinion;
        [DataMember]
        public string expert_opinion_owner;
        [DataMember]
        public string state_expert_opinion;
        [DataMember]
        public string state_export_opinion;
        [DataMember]
        public string comment;
        [DataMember]
        public int year;
        [DataMember]
        public string createdAt;
        [DataMember]
        public string updatedAt;

        public opinion opinionsState => fillOpinion();


        private opinion fillOpinion()
        {
            opinion newOpinion= new opinion();

            newOpinion.expert_opinion = this.expert_opinion;
            newOpinion.export_opinion = this.export_opinion;
            newOpinion.state_expert_opinion = this.state_expert_opinion;
            newOpinion.state_export_opinion = this.state_export_opinion;

            return newOpinion;
        }

            
    }
}
