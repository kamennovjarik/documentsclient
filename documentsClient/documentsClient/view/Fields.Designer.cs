﻿namespace documentsClient.view
{
    partial class Fields
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.authors = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.typeRP = new System.Windows.Forms.TextBox();
            this.typeIP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.day = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.year = new System.Windows.Forms.NumericUpDown();
            this.month = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.protocolNumber = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.source = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.templates = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.authorsRP = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ITypeRP = new System.Windows.Forms.TextBox();
            this.ITypeIP = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.day)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.year)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.protocolNumber)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // authors
            // 
            this.authors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.authors.Location = new System.Drawing.Point(172, 40);
            this.authors.Margin = new System.Windows.Forms.Padding(2);
            this.authors.Name = "authors";
            this.authors.Size = new System.Drawing.Size(514, 20);
            this.authors.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 43);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Авторы именительный падеж";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.typeRP);
            this.groupBox1.Controls.Add(this.typeIP);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(9, 100);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(676, 44);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Тип публикации";
            // 
            // typeRP
            // 
            this.typeRP.Location = new System.Drawing.Point(503, 11);
            this.typeRP.Margin = new System.Windows.Forms.Padding(2);
            this.typeRP.Name = "typeRP";
            this.typeRP.Size = new System.Drawing.Size(169, 20);
            this.typeRP.TabIndex = 3;
            // 
            // typeIP
            // 
            this.typeIP.Location = new System.Drawing.Point(124, 13);
            this.typeIP.Margin = new System.Windows.Forms.Padding(2);
            this.typeIP.Name = "typeIP";
            this.typeIP.Size = new System.Drawing.Size(169, 20);
            this.typeIP.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(381, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Родительный падеж";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Именительный падеж";
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Location = new System.Drawing.Point(9, 181);
            this.title.Margin = new System.Windows.Forms.Padding(2);
            this.title.Multiline = true;
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(677, 99);
            this.title.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 166);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Название публикации";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.day);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.year);
            this.groupBox2.Controls.Add(this.month);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.protocolNumber);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(10, 284);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(676, 44);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Протокол";
            // 
            // day
            // 
            this.day.Location = new System.Drawing.Point(174, 16);
            this.day.Margin = new System.Windows.Forms.Padding(2);
            this.day.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.day.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.day.Name = "day";
            this.day.Size = new System.Drawing.Size(90, 20);
            this.day.TabIndex = 11;
            this.day.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(136, 18);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "День";
            // 
            // year
            // 
            this.year.Location = new System.Drawing.Point(581, 15);
            this.year.Margin = new System.Windows.Forms.Padding(2);
            this.year.Maximum = new decimal(new int[] {
            2030,
            0,
            0,
            0});
            this.year.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.year.Name = "year";
            this.year.Size = new System.Drawing.Size(90, 20);
            this.year.TabIndex = 9;
            this.year.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // month
            // 
            this.month.FormattingEnabled = true;
            this.month.Items.AddRange(new object[] {
            "января",
            "февраля",
            "марта",
            "апреля",
            "мая",
            "июня",
            "июля",
            "августа",
            "сентября",
            "октября",
            "ноября",
            "декабря"});
            this.month.Location = new System.Drawing.Point(331, 14);
            this.month.Margin = new System.Windows.Forms.Padding(2);
            this.month.Name = "month";
            this.month.Size = new System.Drawing.Size(197, 21);
            this.month.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(287, 18);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Месяц";
            // 
            // protocolNumber
            // 
            this.protocolNumber.Location = new System.Drawing.Point(28, 16);
            this.protocolNumber.Margin = new System.Windows.Forms.Padding(2);
            this.protocolNumber.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.protocolNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.protocolNumber.Name = "protocolNumber";
            this.protocolNumber.Size = new System.Drawing.Size(90, 20);
            this.protocolNumber.TabIndex = 6;
            this.protocolNumber.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(552, 17);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Год";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 18);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = " №";
            // 
            // source
            // 
            this.source.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.source.Location = new System.Drawing.Point(9, 443);
            this.source.Margin = new System.Windows.Forms.Padding(2);
            this.source.Multiline = true;
            this.source.Name = "source";
            this.source.Size = new System.Drawing.Size(677, 256);
            this.source.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 428);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(144, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Издание дательный падеж";
            // 
            // templates
            // 
            this.templates.FormattingEnabled = true;
            this.templates.Location = new System.Drawing.Point(60, 10);
            this.templates.Margin = new System.Windows.Forms.Padding(2);
            this.templates.Name = "templates";
            this.templates.Size = new System.Drawing.Size(533, 21);
            this.templates.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(597, 9);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Генерировать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 12);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Шаблон";
            // 
            // authorsRP
            // 
            this.authorsRP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.authorsRP.Location = new System.Drawing.Point(172, 64);
            this.authorsRP.Margin = new System.Windows.Forms.Padding(2);
            this.authorsRP.Name = "authorsRP";
            this.authorsRP.Size = new System.Drawing.Size(514, 20);
            this.authorsRP.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 67);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(150, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Авторы родительный падеж";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ITypeRP);
            this.groupBox3.Controls.Add(this.ITypeIP);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Location = new System.Drawing.Point(10, 355);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(676, 44);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Тип издания";
            // 
            // ITypeRP
            // 
            this.ITypeRP.Location = new System.Drawing.Point(503, 11);
            this.ITypeRP.Margin = new System.Windows.Forms.Padding(2);
            this.ITypeRP.Name = "ITypeRP";
            this.ITypeRP.Size = new System.Drawing.Size(169, 20);
            this.ITypeRP.TabIndex = 3;
            // 
            // ITypeIP
            // 
            this.ITypeIP.Location = new System.Drawing.Point(124, 13);
            this.ITypeIP.Margin = new System.Windows.Forms.Padding(2);
            this.ITypeIP.Name = "ITypeIP";
            this.ITypeIP.Size = new System.Drawing.Size(169, 20);
            this.ITypeIP.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(381, 15);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Родительный падеж";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 18);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Именительный падеж";
            // 
            // Fields
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 710);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.authorsRP);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.templates);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.source);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.title);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.authors);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Fields";
            this.Text = "Fields";
            this.Load += new System.EventHandler(this.Fields_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.day)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.year)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.protocolNumber)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox authors;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox typeRP;
        private System.Windows.Forms.TextBox typeIP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox title;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox month;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown protocolNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown year;
        private System.Windows.Forms.TextBox source;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox templates;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox authorsRP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown day;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox ITypeRP;
        private System.Windows.Forms.TextBox ITypeIP;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}