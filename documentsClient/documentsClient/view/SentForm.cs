﻿using documentsClient.DataSources;
using documentsClient.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace documentsClient.view
{
    public partial class SentForm : Form
    {
        paper curPaper;
        public SentForm(paper paper)
        {
            InitializeComponent();
            curPaper = paper;
        }

        private async void Confirm(object sender, EventArgs e)
        {
            curPaper.state_expert_opinion = "S";
            if (checkBox1.Checked) {
                curPaper.state_expert_opinion = "E";
                curPaper.comment = comment.Text;
            }
            curPaper.expert_opinion_owner = FIO.Text;

            await WebConnector.UpdatePaperAsync(curPaper);

            Close();
        }

        private void SentForm_Load(object sender, EventArgs e)
        {
            title.Text = curPaper.title;
            authors.Text = curPaper.authors;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CommentPane.Visible = checkBox1.Checked;
        }
    }
}
