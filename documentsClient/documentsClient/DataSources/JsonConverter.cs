﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using documentsClient.model;

namespace documentsClient.DataSources
{
    public static class JsonConverter
    {
        //Такой фокус работает только с сериализуемыми классами (см класс paper)
        public static List<paper> ParcePapersFromJSON(string jsonReq)
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(List<paper>));
            Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(jsonReq));

            return ((List<paper>)jsonFormatter.ReadObject(stream));
        }

        public static string paperToJSON(paper _paper)
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(paper));

            Stream stream = new MemoryStream();
            jsonFormatter.WriteObject(stream, _paper);
            stream.Position = 0;
            var sr = new StreamReader(stream);

            var ret = sr.ReadLine();

            return ret;
        }
    }
}
