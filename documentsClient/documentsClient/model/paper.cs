﻿using System.Drawing;
using System.Runtime.Serialization;

namespace documentsClient.model
{
    [DataContract]
    public class paper
    {
        [DataMember]
        public int id;
        [DataMember]
        public string title;
        [DataMember]
        public string source;
        [DataMember]
        public string authors;
        [DataMember]
        public string link;
        [DataMember]
        public string expert_opinion;
        [DataMember]
        public string expert_opinion_owner;
        [DataMember]
        public string state_expert_opinion;
        [DataMember]
        public string comment;
        [DataMember]
        public int year;
        [DataMember]
        public string createdAt;
        [DataMember]
        public string updatedAt;

        public string hasExpertOpinion => getState();
        public Color hasExpertOpinionColor => getColor();

        public string getState() {
            if (state_expert_opinion == "N")
            {
                return "Отсутствует";
            }

            if (state_expert_opinion == "U")
            {
                return "Сгенерировано";
            }

            if (state_expert_opinion == "S")
            {
                return "Сдано";
            }

            return "Не требуется";
        }

        public Color getColor()
        {
            if (state_expert_opinion == "N")
            {
                return Color.Red;
            }

            if (state_expert_opinion == "U")
            {
                return Color.Yellow;
            }

            if (state_expert_opinion == "S")
            {
                return Color.Green;
            }

            return Color.Blue;
        }
    }
}
